/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author LightMind
 */
public class NNElmt {
    public int FrameNumber;
    public long timepacket;
    public int isAttack;
    public String verdict;
    public int port_src;
    public int port_dst;
    public String ip_src;
    public String ip_dst;
    public String jenis_paket;
    public int jumlah_paket;
    public int payload_size;
    public ArrayList<String> tcpFlags = new ArrayList<>();
    public int jumlah_SYN=0;
    public int jumlah_RST=0;
    public int jumlah_FIN=0;
    private final int weight = 1;
    
    public static Comparator<NNElmt> attackComp = (NNElmt t, NNElmt t1) -> t1.verdict.length() - t.verdict.length();
   
    private String convertFlags(){
        String hasil = "";
        for (String F : tcpFlags){
            hasil = hasil + F + " ";
        }
        return hasil;
    }
    
    @Override
    public String toString(){
        return FrameNumber+" "+timepacket+" "+port_src+" "+port_dst+" "+ip_src+" "+ip_dst+" "+jenis_paket+" "+jumlah_paket+" "+payload_size+" "+convertFlags();
    }

    public int getPortClass(int p){
        if ((p > 0) && (p < 1023)) return 1;
        else if ((p > 1024) && (p < 49151)) return 2;
        else return 3;
    }
    
    public int compareNNElmt(NNElmt input){
        int skor = 0;
        boolean isSequence = true;
        /* F8 */
        /*
        if (this.ip_src.equals(input.ip_src)) skor+=weight;
        if (this.ip_dst.equals(input.ip_dst)) skor+=weight;
        
        if (getPortClass(this.port_src) == getPortClass(input.port_src)) skor+=weight;
        if (getPortClass(this.port_dst) == getPortClass(input.port_dst)) skor+=weight;
        */
        if (this.jumlah_paket == input.jumlah_paket) skor+=weight;
        if (this.payload_size == input.payload_size) skor+=weight;
                
        if (this.jenis_paket.equals(input.jenis_paket)) skor+=weight;
        
        
        int inputFlagSize = input.tcpFlags.size();
        
        for (int i=0; i<tcpFlags.size(); i++){
            if (i == inputFlagSize) break;
            else {
                if (tcpFlags.get(i).equals(input.tcpFlags.get(i))) skor+=weight; //dikali 2 apakah lebih baik?
                else isSequence = false;
            }
        } 
        
        if (isSequence) skor+=weight; //jika sesuai sequence, maka +1, dikali 2 173665
        
        
        if (input.jumlah_FIN == this.jumlah_FIN) skor+=weight;
        if (input.jumlah_SYN == this.jumlah_SYN) skor+=weight;
        if (input.jumlah_RST == this.jumlah_RST) skor+=weight;
        
        
        return skor;
    }
}
