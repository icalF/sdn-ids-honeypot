/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import Settings.GlobalSettings;
import Settings.ShutdownHook;
import Settings.StopThread;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;  
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;  
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jnetpcap.Pcap;  
import org.jnetpcap.PcapIf; 
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author LightMind
 */
public class SnifferApp {    
    private static ArrayList<NNElmt> NearestNeighbourData = new ArrayList<>();
    private static NNElmt LastSession = new NNElmt();
    public static int JmlPaket = 0;
    private static boolean isDialihkan = false;
    public static double JmlDelay = 0;
    public static double JmlDelayKuadrat = 0;
    //private static int MaxRecvPacket = 100;
    public static double MaxDelay = 0;
    public static double MinDelay = 999999;
    private static String Controller_API = "http://10.42.0.1:8080/";
    private static Scanner sc = new Scanner(System.in);
    private static String DPID; private static String production_port; private static String honeypot_port;
    
    private static void FlowManagementWithIP(String DPID, String in_port, String out_port, String act, String ip_src){
		String jsonString;
		
		if (in_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("hard_timeout", 60)
	                  .put("match", new JSONObject()
	                		  		.put("in_port","LOCAL")
	                		  		.put("ipv4_src", ip_src)
	                		  		.put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		} else if (out_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("hard_timeout", 60)
	                  .put("match", new JSONObject()
  	                		    .put("in_port",Integer.parseInt(in_port))
	                		  	.put("ipv4_src", ip_src)
	                		  	.put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", "LOCAL")
	                		  )
	                  )
	                  .toString();
		} else { //pasti bukan local
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("hard_timeout", 60)
	                  .put("match", new JSONObject()
	                		  .put("in_port",Integer.parseInt(in_port))
	                		  .put("ipv4_src", ip_src)	 
	                		  .put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		}
		
		System.out.println("KONEKSI DIALIHKAN...");
		if (act.equals("add")) postRequest(Controller_API+"stats/flowentry/add",jsonString);
		else if (act.equals("modify")) postRequest(Controller_API+"stats/flowentry/modify",jsonString);
		else System.out.println("Not Implemented Yet");
	}
    
    private static void FlowManagement(String DPID, String in_port, String out_port, String act){
		String jsonString;
		
		if (in_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port","LOCAL")
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		} else if (out_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port",Integer.parseInt(in_port))
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", "LOCAL")
	                		  )
	                  )
	                  .toString();
		} else { //pasti bukan local
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port",Integer.parseInt(in_port))
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		}
		
		//System.out.println(jsonString);
		if (act.equals("add")) postRequest(Controller_API+"stats/flowentry/add",jsonString);
		else if (act.equals("modify")) postRequest(Controller_API+"stats/flowentry/modify",jsonString);
		else System.out.println("Not Implemented Yet");
	}
    
    private static String getRequest(String URL){
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(URL);

			// add request header
			request.addHeader("User-Agent", "SDN Controller Java");
			HttpResponse response = client.execute(request);

			BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			return result.toString();
			
		  } catch (Exception e){e.printStackTrace(); return null;}
	}
	
	private static String postRequest(String URL, String data){
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(URL);

			// add header
			post.setHeader("User-Agent", "SDN Controller Java");
			//post.addHeader("content-type", "application/x-www-form-urlencoded");
			
			//set Data
			post.setEntity(new StringEntity(data));

			HttpResponse response = client.execute(post);
			//System.out.println("Response Code : " 
		                //+ response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			return result.toString();
		} catch (Exception e){e.printStackTrace(); return null;}
	}
    private static void resetLastSession(){
        LastSession.timepacket = 0;
        LastSession.ip_src = "";
        LastSession.ip_dst = "";
        LastSession.port_dst = 0;
        LastSession.port_src = 0;
        LastSession.jenis_paket = "";
        LastSession.jumlah_paket = 0;
        LastSession.payload_size = 0;
        LastSession.tcpFlags = new ArrayList<String>();
    }
    
    private static void setLastSession(long tp, String ip_src, String ip_dst, int port_src, int port_dst, String jenis_paket, int payload_size, String tcpFlags){
        LastSession.timepacket = tp;
        LastSession.ip_src = ip_src;
        LastSession.ip_dst = ip_dst;
        LastSession.port_src = port_src;
        LastSession.port_dst = port_dst;
        LastSession.jenis_paket = jenis_paket;
        LastSession.jumlah_paket = 1;
        LastSession.payload_size = payload_size;
        LastSession.tcpFlags.add(tcpFlags);
    }
    
    private static void addLastSession(int payload_size, String tcpFlags){
        LastSession.payload_size += payload_size;
        LastSession.tcpFlags.add(tcpFlags);
        LastSession.jumlah_paket++;
    }
    
    private static void readDataSet() throws FileNotFoundException, IOException{
        String FILE_NAME;
        FILE_NAME = GlobalSettings.learn_dir+"dataset-full.learn";
       
        
        FileReader file_to_read;
        file_to_read = new FileReader(FILE_NAME);
        BufferedReader bf = new BufferedReader(file_to_read);
        String aLine;
        String lompat = "";
        
        while ((aLine = bf.readLine()) != null){
            NNElmt NN = new NNElmt();
            StringTokenizer defaultTokenizer = new StringTokenizer(aLine);  
            NN.FrameNumber = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            lompat = defaultTokenizer.nextToken();
            NN.timepacket = Integer.parseInt(defaultTokenizer.nextToken());
            lompat = defaultTokenizer.nextToken();
            String temp_verdict = defaultTokenizer.nextToken();
            NN.port_src = Integer.parseInt(defaultTokenizer.nextToken());
            NN.port_dst = Integer.parseInt(defaultTokenizer.nextToken());
            NN.ip_src = defaultTokenizer.nextToken();
            NN.ip_dst = defaultTokenizer.nextToken();
            NN.jenis_paket = defaultTokenizer.nextToken();
            NN.jumlah_paket = Integer.parseInt(defaultTokenizer.nextToken());
            NN.payload_size = Integer.parseInt(defaultTokenizer.nextToken());
            while (defaultTokenizer.hasMoreTokens()){
                String str_flag = defaultTokenizer.nextToken();
                if ("FIN".equals(str_flag)) NN.jumlah_FIN++;
                if ("SYN".equals(str_flag)) NN.jumlah_SYN++;
                if ("RST".equals(str_flag)) NN.jumlah_RST++;
                NN.tcpFlags.add(str_flag);
            }
            
            for (String retval: temp_verdict.split(",")){ //jika multilabel
                NN.verdict = retval;
                NearestNeighbourData.add(NN);
            }
        }
        bf.close();
        System.out.println("Terbaca: "+FILE_NAME);
    }
    
    public static String determineVerdict(ArrayList<NearestNeighbourResult> in){
        HashMap<String, Integer> ranking = new HashMap<>();
        
        int count = 0;
        int batas = in.size();
        
        if (GlobalSettings.MaximumNeighbour < in.size()) batas = GlobalSettings.MaximumNeighbour;
        
        while (count < batas) {   
            String v = in.get(count).verdict;
            if (ranking.get(v) == null){
                ranking.put(v, 1);
            } else {
                int value = ranking.get(v);
                value++;
                ranking.put(v, value);
            }
            count++;
        }
        
        String hasil_attack="-";
        int jumlah_vote=0;

        Set<Entry<String, Integer>> set = ranking.entrySet();
        // Get an iterator
        Iterator<?> i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            int value = (Integer) me.getValue();
            if (value > jumlah_vote){
                jumlah_vote = value;
                hasil_attack = (String) me.getKey();
            }
        }
        return hasil_attack;
    }
    
    
    private static ArrayList<NearestNeighbourResult> getAllScores(){
        ArrayList<NearestNeighbourResult> res = new ArrayList<>();
        for (NNElmt ne : NearestNeighbourData){
            NearestNeighbourResult NNR = new NearestNeighbourResult();
            NNR.ip_dst = ne.ip_dst;
            NNR.ip_src = ne.ip_src;
            NNR.port_src = ne.port_src;
            NNR.port_dst = ne.port_dst;
            NNR.verdict = ne.verdict;
            NNR.skor = ne.compareNNElmt(LastSession); //kan ada skor
            res.add(NNR);
        }
        Collections.sort(res, NearestNeighbourResult.skorComp); 
        return res;
    }
    
    public static void initSDNController(){
    	System.out.println("RYU SDN Controller Configuration... ");
        System.out.print("Input your Switch DPID: "); 
		DPID = sc.next();
		System.out.print("Specify production server port: ");
		production_port = sc.next();
		System.out.print("Specify honeypot server port: ");
		honeypot_port = sc.next();
		
		FlowManagement(DPID, production_port, "LOCAL", "add");
		FlowManagement(DPID, honeypot_port, "LOCAL", "add");
		FlowManagement(DPID, "LOCAL", production_port, "add");
    }
    
    public static void main(String args[]) throws IOException {
    	try {
    		System.loadLibrary("jnetpcap");
    		System.out.println("The native libraries jnetpcap are installed.");
    		System.out.println(System.getProperty("java.library.path"));
    	} catch (Exception e) {
    		System.out.println("Masuk Error");
    		System.out.println(e);
    		System.exit(-1);
        }
    	
        String switches = getRequest(Controller_API+"stats/switches");
        if (switches != null) {
        	System.out.println("Switches: "+switches);
        	initSDNController();
        	
	    	Runtime.getRuntime().addShutdownHook(new ShutdownHook());
	        StopThread st = new StopThread();
	        st.start();
			
	        long tStart = System.currentTimeMillis();
	        
	        readDataSet();
	        System.out.println("Sorting");
	        Collections.sort(NearestNeighbourData, NNElmt.attackComp); //masalah disini
	        
	        long tEnd = System.currentTimeMillis();
	        long tDelta = tEnd - tStart;
	        System.out.println("Dataset selesai dibaca: "+NearestNeighbourData.size());
	        System.out.println("Waktu untuk membaca dataset: "+(tDelta / 1000.0)+" detik");
	        //System.exit(1);
	        
	        
	        List<PcapIf> alldevs = new ArrayList<PcapIf>(); // Will be filled with NICs  
	        StringBuilder errbuf = new StringBuilder();     // For any error msgs  
	        
	        /*************************************************************************** 
	        * First get a list of devices on this system 
	        **************************************************************************/  
	       int r = Pcap.findAllDevs(alldevs, errbuf);  
	       if (r == Pcap.NOT_OK || alldevs.isEmpty()) {  
	         System.err.printf("Can't read list of devices, error is %s\n",   
	           errbuf.toString());  
	         System.exit(-1);
	       }         
	       System.out.println("Network devices found:");  
	        int i = 0;  
	        for (PcapIf device : alldevs) {  
	            String description =  
	                (device.getDescription() != null) ? device.getDescription()  
	                    : "No description available";  
	            System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);  
	        }  
	        
	        PcapIf device = alldevs.get(2); // Listen to all interfaces
	        System.out.printf("\nChoosing '%s' on your behalf:\n",  (device.getDescription() != null) ? device.getDescription() : device.getName());  
	        
	        /*************************************************************************** 
	        * Second we open up the selected device 
	        **************************************************************************/  
	       int snaplen = 64 * 1024;           // Capture all packets, no trucation  
	       int flags = Pcap.MODE_PROMISCUOUS; // capture all packets  
	       int timeout = 10 * 1000;           // 10 seconds in millis  
	       Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, errbuf);  
	       if (pcap == null) {  
	        System.err.printf("Error while opening device for capture: %s\n",   
	           errbuf.toString());  
	         return;  
	       }
	        
	        PcapPacketHandler<String> jpacketHandler = new PcapPacketHandler<String>() {
	        	public void nextPacket(PcapPacket packet, String user){
	        		pcapdump(packet);
	        	}
	        };
	        
	        /*************************************************************************** 
	         * Fourth we enter the loop and tell it to capture 10 packets. The loop 
	         * method does a mapping of pcap.datalink() DLT value to JProtocol ID, which 
	         * is needed by JScanner. The scanner scans the packet buffer and decodes 
	         * the headers. The mapping is done automatically, although a variation on 
	         * the loop method exists that allows the programmer to sepecify exactly 
	         * which protocol ID to use as the data link type for this pcap interface. 
	         **************************************************************************/  
	        pcap.loop(Pcap.LOOP_INFINITE, jpacketHandler, "jNetPcap rocks!");  
	  
	        /*************************************************************************** 
	         * Last thing to do is close the pcap handle 
	         **************************************************************************/  
	        pcap.close();
        } else {
        	System.err.println("Make sure you have connection to SDN Controller!");
        	System.exit(-1);
        }
        
    }
    
    private static void pcapdump(PcapPacket packet) {
        Ip4 ip = new Ip4();  
        Ethernet eth = new Ethernet();  
        Tcp tcp = new Tcp();
        Udp udp = new Udp();
        Icmp icmp = new Icmp();
        
        int port_src = -1;
        int port_dst = -1;
        int payload_size = 0;
        String tcpFlags = "";
        String ip_src = "-";
        String ip_dst = "-";
        String jenis_paket = "-";
        String mac_src = "-";
        String mac_dst = "-";
        
        long tp = packet.getCaptureHeader().timestampInMillis();
        String tanggal = new Date(tp).toString();
        
        if (packet.hasHeader(eth)){
            mac_src = FormatUtils.mac(eth.source()); 
            mac_dst = FormatUtils.mac(eth.destination());
        }
        //honeypot 1, production 2
        if (packet.hasHeader(ip)){
                ip_src = org.jnetpcap.packet.format.FormatUtils.ip(packet.getHeader(ip).source());
                ip_dst = org.jnetpcap.packet.format.FormatUtils.ip(packet.getHeader(ip).destination());
                payload_size = ip.getOffset() + ip.size();
        }
        
        if (packet.hasHeader(tcp)){
                port_src = packet.getHeader(tcp).source();
                port_dst = packet.getHeader(tcp).destination();
                jenis_paket = "TCP";
                for (Tcp.Flag cntrlFlag : packet.getHeader(tcp).flagsEnum()) {
                    tcpFlags = tcpFlags + cntrlFlag.toString() + " ";
                }
                if (tcpFlags.length() != 0) tcpFlags = tcpFlags.substring(0,tcpFlags.length()-1);
        }
        
        if (packet.hasHeader(udp)){
                port_src = packet.getHeader(udp).source();
                port_dst = packet.getHeader(udp).destination();
                jenis_paket = "UDP";
        }
        
        if (packet.hasHeader(icmp)){
                jenis_paket = "ICMP";
                port_src = 0;
                port_dst = 0;
        }
        
        
        if ((!ip_src.equals("-")) && (!ip_dst.equals("-")) && (!jenis_paket.equals("-"))) {            
            if (LastSession.timepacket != 0){
                if (((tp == LastSession.timepacket) && (port_src == LastSession.port_src) && (port_dst == LastSession.port_dst)) || ((tp == LastSession.timepacket) && (port_src == LastSession.port_dst) && (port_dst == LastSession.port_src))) {
                    addLastSession(payload_size, tcpFlags); //tambah ke session yang ada
                } else { //kalau sudah beda
                    JmlPaket++;
                    
                    //Klasifikasikan session tersebut
                    
                    long tStart = System.currentTimeMillis();
                    String verdict = determineVerdict(getAllScores());
                    long tEnd = System.currentTimeMillis();
                    long tDelta = tEnd - tStart;
                    
                    if (verdict.equals(("-"))) {
                    	System.out.println(JmlPaket+"-"+tanggal+" "+tp+" "+mac_src+" "+mac_dst+" "+ip_src+" "+ip_dst+" "+port_src+" "+port_dst+" "+jenis_paket+" "+payload_size+" "+tcpFlags);
                    } //tentukan verdict
                    else {
                    	System.out.println(JmlPaket+"-"+tanggal+" "+tp+" "+mac_src+" "+mac_dst+" "+ip_src+" "+ip_dst+" "+port_src+" "+port_dst+" "+jenis_paket+" "+payload_size+" "+tcpFlags);
                    	//FlowManagement(DPID, "LOCAL", honeypot_port, "modify");
                    	pengalihanKoneksi(DPID, honeypot_port);
                    	System.out.print(LastSession);
                    	System.out.println(" : attack ("+ip_src+", dst: "+ip_dst+") MAC ("+mac_src+","+mac_dst); //tentukan verdict, controller SDN disini
                    	System.out.println("Delay: "+tDelta);
                    }
                    
                    if (JmlPaket == 8) pengalihanKoneksi(DPID, honeypot_port);
                    if (JmlPaket == 20) FlowManagement(DPID, "LOCAL", production_port, "add");
                    
                    if (MaxDelay < tDelta) MaxDelay = tDelta;
                    if (MinDelay > tDelta) MinDelay = tDelta;
                    
                    JmlDelay = JmlDelay + tDelta;
                    JmlDelayKuadrat = JmlDelayKuadrat + Math.pow(tDelta, 2);
                    
                    resetLastSession(); //reset
                    setLastSession(tp,ip_src, ip_dst, port_src, port_dst, jenis_paket, payload_size, tcpFlags); //reset
                }
            } else setLastSession(tp,ip_src, ip_dst, port_src, port_dst, jenis_paket, payload_size, tcpFlags); //set baru
        }
        
    }
    
    private static void pengalihanKoneksi(String DPID, String honeypot_port){
    	if (!isDialihkan){
    		FlowManagementWithIP(DPID, "LOCAL", honeypot_port, "add", "192.168.90.2");
    		FlowManagement(DPID, "LOCAL", honeypot_port, "add");
    		isDialihkan = true;
    	}
    }
}
