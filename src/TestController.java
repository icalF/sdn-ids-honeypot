import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestController {
	private static String Controller_API = "http://10.42.0.1:8080/";
	private static Scanner sc;
	
	private static String getRequest(String URL){
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(URL);

			// add request header
			request.addHeader("User-Agent", "SDN Controller Java");
			HttpResponse response = client.execute(request);

			BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			return result.toString();
			
		  } catch (Exception e){e.printStackTrace(); return null;}
	}
	
	private static String postRequest(String URL, String data){
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(URL);

			// add header
			post.setHeader("User-Agent", "SDN Controller Java");
			//post.addHeader("content-type", "application/x-www-form-urlencoded");
			
			//set Data
			post.setEntity(new StringEntity(data));

			HttpResponse response = client.execute(post);
			System.out.println("Response Code : " 
		                + response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(
			        new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			return result.toString();
		} catch (Exception e){e.printStackTrace(); return null;}
	}
	
	private static String deleteRequest(String URL){
		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			HttpDelete deleteRequest = new HttpDelete(URL);
			deleteRequest.setHeader("Accept", "application/json");

			HttpResponse response = httpClient.execute(deleteRequest);
			String status = response.getStatusLine().toString();
			return status;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private static void getSwitches(){
		System.out.println(getRequest(Controller_API+"stats/switches"));
	}
	
	private static void getSwitchDesc(String DPID){
		System.out.println(getRequest(Controller_API+"stats/desc/"+DPID));
	}
	
	private static void getFlowDesc(String DPID){
		System.out.println(getRequest(Controller_API+"stats/flow/"+DPID));
	}
	
	private static void getMeterDesc(String DPID){
		System.out.println(getRequest(Controller_API+"stats/meter/"+DPID));
	}
	
	private static void getPortDesc(String DPID){
		System.out.println(getRequest(Controller_API+"stats/port/"+DPID));
	}
	
	private static void FlowManagement(String DPID, String in_port, String out_port, String act){
		String jsonString;
		
		if (in_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port","LOCAL")
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		} else if (out_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port",Integer.parseInt(in_port))
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", "LOCAL")
	                		  )
	                  )
	                  .toString();
		} else { //pasti bukan local
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 11111)
	                  .put("match", new JSONObject()
	                		  	    .put("in_port",Integer.parseInt(in_port))
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		}
		
		System.out.println(jsonString);
		if (act.equals("add")) postRequest(Controller_API+"stats/flowentry/add",jsonString);
		else if (act.equals("modify")) postRequest(Controller_API+"stats/flowentry/modify",jsonString);
		else System.out.println("Not Implemented Yet");
	}
	
	private static void FlowManagementWithIP(String DPID, String in_port, String out_port, String act, String ip_src){
		String jsonString;
		
		if (in_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("match", new JSONObject()
	                		  		.put("in_port","LOCAL")
	                		  		.put("ipv4_src", ip_src)
	                		  		.put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		} else if (out_port.equalsIgnoreCase("local")) {
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("match", new JSONObject()
  	                		    .put("in_port",Integer.parseInt(in_port))
	                		  	.put("ipv4_src", ip_src)
	                		  	.put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", "LOCAL")
	                		  )
	                  )
	                  .toString();
		} else { //pasti bukan local
			jsonString = new JSONObject()
	                  .put("dpid", DPID)
	                  .put("priority", 22222)
	                  .put("match", new JSONObject()
	                		  .put("in_port",Integer.parseInt(in_port))
	                		  .put("ipv4_src", ip_src)	 
	                		  .put("eth_type", 2048)
	                  )
	                  .put("actions", new JSONArray().put(
	                		  new JSONObject()
	                		  	.put("type", "OUTPUT")
	                		  	.put("port", Integer.parseInt(out_port))
	                		  )
	                  )
	                  .toString();
		}
		
		System.out.println(jsonString);
		if (act.equals("add")) postRequest(Controller_API+"stats/flowentry/add",jsonString);
		else if (act.equals("modify")) postRequest(Controller_API+"stats/flowentry/modify",jsonString);
		else System.out.println("Not Implemented Yet");
	}
	
	private static void deleteAllFlow(String DPID){
		System.out.println("Delete All Flow: "+deleteRequest(Controller_API+"stats/flowentry/clear/"+DPID));
	}
	
	private static void printBanner(){
		System.out.println("Welcome to Ryu Controller Demo");
		System.out.println("=============================================");
		System.out.println("1. List All Switches");
		System.out.println("2. Switch Description");
		System.out.println("3. Flow Switch Description");
		System.out.println("4. Meter Switch Description");
		System.out.println("5. Port Switch Description");
		System.out.println("6. Add Flow (in/out port only)");
		System.out.println("7. Modify Flow (in/out port only)");
		System.out.println("8. Delete All Flow");
		System.out.println("9. Add Flow with IP (in port only)");
		System.out.println("0. Quit");
		System.out.println("=============================================");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int cmd = 1;
		sc = new Scanner(System.in);
		while (cmd != 0){
			if (cmd == 1) getSwitches();
			else if (cmd == 2) {
				System.out.print("Input your DPID: "); 
				getSwitchDesc(sc.next());
			} else if (cmd == 3) {
				System.out.print("Input your DPID: "); 
				getFlowDesc(sc.next());
			} else if (cmd == 4) {
				System.out.print("Input your DPID: "); 
				getMeterDesc(sc.next());
			} else if (cmd == 5) {
				System.out.print("Input your DPID: "); 
				getPortDesc(sc.next());
			} else if (cmd == 6){
				System.out.print("Input your DPID: "); 
				String DPID = sc.next();
				System.out.print("Specify in port match: ");
				String in_port = sc.next();
				System.out.print("Specify out port match: ");
				String out_port = sc.next();
				FlowManagement(DPID, in_port, out_port, "add");
			} else if (cmd == 7){
				System.out.print("Input your DPID: "); 
				String DPID = sc.next();
				System.out.print("Specify in port match: ");
				String in_port = sc.next();
				System.out.print("Specify out port match: ");
				String out_port = sc.next();
				FlowManagement(DPID, in_port, out_port, "modify");
			} else if (cmd == 8) {
				System.out.print("Input your DPID: "); 
				deleteAllFlow(sc.next());
			} 
			else if (cmd == 9){
				System.out.print("Input your DPID: "); 
				String DPID = sc.next();
				System.out.print("Alamat IP: "); 
				String ip_src = sc.next();
				System.out.print("Specify in port match: ");
				String in_port = sc.next();
				System.out.print("Specify out port match: ");
				String out_port = sc.next();
				FlowManagementWithIP(DPID, in_port, out_port, "add", ip_src);
			}
			else System.out.println("Your choice is not recognized");
			printBanner();
			
			System.out.print("Choice: ");
			cmd = sc.nextInt();
		}
	}

}
